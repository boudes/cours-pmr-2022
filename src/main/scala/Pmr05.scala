package fr.mipn.pmr05

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util.{Success, Failure}
import java.util.concurrent.Executors

object Pmr05 extends App {
    implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(8))

    val fut = Future {
        val x = 2 / 0
        12
    }

    fut.map(_ + 1).onComplete {
        case Success(n) => println(s"Bravo c'est un succès $n")
        case Failure(e) => println(s"echec $e")
    }

    val a = Future { 2 }
    val b = Future { 3 }
    val c = Future { 4 }
    val sum = for {
            va <- a
            vb <- b
            vc <- c
    } yield (va + vb + vc) 

    println(sum.isCompleted) // not completed

    sum.map(_ + 1).onComplete {
        case Success(n) => println(s"la somme est $n")
        case Failure(e) => println(s"echec $e")
    }

    Thread.sleep(20000) // wait 20 seconds and …
    println("Sayonara")
    ec.shutdown()       // … shutdown (stop) the application
}

   