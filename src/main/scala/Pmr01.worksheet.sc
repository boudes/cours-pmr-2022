
/* prélude à l'exercice sur les anagrammes */
def split(word: String): Map[Char,Int] = word.filterNot(_ == ' ').toList.groupBy((x) => x).view.mapValues(_.length).toMap

split("hello")
split("vélo") 
split("lové")
split("volé")

val smalllexic = Vector("vélo", "volé", "lové", "oo", "ooo")

val table = smalllexic.groupBy(split)
table.get(split(""))

/* prélude (2) en décomposant des entiers */
val tokens = Set(2,5)
def decompose(n: Int): Set[List[Int]] = if (0 == n) Set(List()) 
else tokens.filter(_ <= n).flatMap  {
    (token) => decompose(n - token).map((solution) => (token::solution).sorted)
}

decompose(7)
decompose(1)

/* Du cours : Option, Either, Try */
var x: Option[Int] = Some(5)
x = None
var y = Option(6)
Option(null)

var z:Either[Int, String] = Left(4)
z = Right("bonjour")
z  

import scala.util.Try
def f(x: Int) = Try(1 - 100/x).toOption 
f(1)
f(0)

/* les "streams" renommés en lazylist */
def from(n: Int): LazyList[Int] = n #:: from(n + 1)
from(5)
def sieve(s: LazyList[Int]): LazyList[Int] =
  s.head #:: sieve(s.tail.filter(_ % s.head != 0))

sieve(from(2)).take(80).foreach(println)


class Animal(val height: Double, val name: String) {
    def walk(): Unit = println(s"${name} walks")
}

val dragon = new Animal(3.2, "Eliott")

object Soleil {
    var distance_a_la_Terre = 150_000_000_000.0
    private val parfum_de_glace_prefere = "Yahourt"
    def brille() = println("le Soleil brille")
}

Soleil.brille()
Soleil.distance_a_la_Terre *= 2
Soleil.distance_a_la_Terre


