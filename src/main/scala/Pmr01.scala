package fr.mipn.pmr01
object Main extends App {
  println("Cours PMR 01")

  val tokens = List(2,5)

  def decompose(n: Int): List[List[Int]] = if (0 == n) List(List()) 
  else tokens.filter(_ <= n).flatMap  {
      (token) => decompose(n - token).map(token::_)
  }

  /* objet compagnon */

  object Animal {
    def apply(height: Double, name: String) = new Animal(height, name)
    def apply(name: String) = new Animal(10, name)
    private var parfum_prefere = "Vanille"
  }

  class Animal(val height: Double, val name: String) {
    def walk(): Unit = println(s"${name} walks")
    def prefer(s: String) = Animal.parfum_prefere = s
    def icecream() = println(s"Je préfère ${Animal.parfum_prefere}")
  }

  val eliott = Animal("Eliott")
  println(eliott.height)
  val oscar = Animal("Oscar")
  eliott.prefer("fraise")
  oscar.icecream()


 /* autre exemple de compagnon/singleton */
 
}