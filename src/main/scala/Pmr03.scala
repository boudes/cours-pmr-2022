package fr.mipn.pmr03

sealed trait ExpAlg
case object Zero extends ExpAlg
case class Variable(nom: String) extends ExpAlg
case class Somme(e1: ExpAlg, e2: ExpAlg) extends ExpAlg

trait PrettyPrinter {
    def expalgToString(e: ExpAlg): String = e match {
    case Zero => "0"
    case Somme( Somme(e1,e2), Somme(e3, e4)) => "(" + expalgToString(Somme(e1, e2)) + ") + (" +
    expalgToString(Somme(e3, e4)) + ")"
    case Somme( Somme(e1,e2), e3) => "(" + expalgToString(Somme(e1, e2)) + ") + " +
    expalgToString(e3)
    case Somme(e1, e2) => expalgToString(e1) + " + " +
    expalgToString(e2)
    case Variable (nom) => nom
  } 
}

object Freak {
    def unapply(f: Freak): Option[(Int, String)] = Some((42,"coucou")) 
    def apply(n:Int, name: String) = new Freak(43, name)
}

class Freak(n: Int, name: String) {
    def walk()= println(s"${name} ${n} walk")
}

object Truc {
    def unapply(f: Truc): Option[(Int, String)] = Some((100,"coucou")) 
}

class Truc(a: Int,b: String) extends Freak(a,b)

object Pmr03 extends App with PrettyPrinter {
    println("Hello !")
    val e = Somme(Zero, Variable("x"))
    println(expalgToString(e))
    println(expalgToString(Somme(Somme(Zero, Zero), Zero)))
    println(expalgToString(Somme(Somme(Zero, Zero), Somme(Zero, Zero))))
    println(expalgToString(Somme(Zero, Somme(Zero, Zero))))

    val f = Freak(20, "Bob")
    f.walk()
    val Freak(age, _) = f // Freak.unapply "menteur"
    println(age) 
    val g = new Truc(10, "Bill")
    val Freak(x, y) = g // appelle de Freak.unapply sur un Truc
    println(x)
} 
