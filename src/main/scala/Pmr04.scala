package fr.mipn.pmr04

object Pmr04 extends App {
  import scala.io.Source
  val bufflexique = Source.fromResource("lexique.txt")
  // val bufflexique = Source.fromFile("/home/pierre/Documents/contrib/PMR_2022/anagrams/src/main/resources/lexique.txt","utf-8")
  val lexique = bufflexique.getLines.toList
  println(s"Le lexique contient ${lexique.size} mots")
  bufflexique.close() 

  /* exercice préliminaire 
  val tokens = List(5,2)

  def decomposeInt(x: Int): List[List[Int]] = 
    if (x == 0) List(List()) 
    else  tokens.flatMap {
      (token) => if (x >= token) decomposeInt(x - token).map(token::_) else List() 
    }
  println(decomposeInt(10))
  */

  def split(word: String): Map[Char,Int] = word.filterNot(_ == ' ').toList.groupBy((x) => x).view.mapValues(_.length).toMap
 
  val table = lexique.groupBy(split)
  val keys = table.keys.toVector
  println(s"La table des mots du français contient ${lexique.size} mots répartis en ${keys.length} entrées")


}
